---
permalink: /
---
Welcome to the Codeberg Documentation pages! We're here to help you when you're having issues or questions with Codeberg.

Please choose a section from the sidebar on the left, which you can toggle by clicking the three stripes at the top left.

If you're new to Codeberg, consider reading the [Getting Started Guide](/getting-started).

## Getting more help

If you encounter a bug, your question isn't answered here, or you just can't find the answer, feel free to open a [Codeberg Community Issue](https://codeberg.org/Codeberg/Community/issues). This is the preferred way to report bugs to the software or ask questions if you can't find an answer somewhere else. You can also quickly find the Codeberg Issues via the dropdown in the navbar.

There are more [ways to chat with us or the community](/contact/), in pretty much all places are nice people around to help you with your experience.

