---
eleventyNavigation:
  key: UsingLists
  title: Using Lists
  parent: Markdown
  order: 60
---

You can use lists in your Markdown files.

## Unordered lists

To use an unordered list (bullet points), simply begin your list items with a dash `-` or a star `*`.

For example:

```
- This
- is
- a
- simple
- bullet
- point
- list
```

This gets rendered as:

- This
- is
- a
- simple
- bullet
- point
- list

## Numbered lists

To use a numbered list, simply begin your list items with a number.

```shell
1. This
2. is
3. a
4. numbered
5. list
```

Gets rendered as:

1. This
2. is
3. a
4. numbered
5. list

Note that the numbers do not have to be in the correct order (even though it is easier to read in the non-rendered form):

```
1. This
1. is
1. also
1. a
1. numbered
1. list
```

will also render to a correctly numbered list:

1. This
1. is
1. also
1. a
1. numbered
1. list

Some editors even autocorrect this to a correctly numbered list. (If the example above does no longer start with `1.`
on each line, please feel free to reintroduce the mistake for illustration purposes.)
